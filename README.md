# Switch Manager Front

Front-end of the Switch Manager project.

## Installation

To install the project, you first need to install angular material javascript modules through npm :

```
cd static && npm install
```

This project also require Python Flask module :

```
pip install --user flask  # If you want to install flask for all users, remove the '--user'
```

## Run the project

To run the project, simply run the `app.py` :

```
python app.py
```

# Roadmap

- ~~Generate a angular material based front-end~~
- ~~Buildings / Floors / Rooms visualization~~
- ~~Plug selection~~
- Configuration generation from plug selection
- Configuration listing, editing and scheduling
- Planning visualization and edition
- Mapping modification

# Author

Axel Martin
