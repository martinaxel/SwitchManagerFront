(function () {

    angular
        .module('mapping')
        .service('FloorService', FloorService);

    function FloorService($q) {

        /* FIXME: should be server side */

        var floors = [
            {
                name: 'Floor 1',
                id: 1,
                buildingId: 0
            },
            {
                name: 'Floor 2',
                id: 2,
                buildingId: 0
            }
        ];

        /* Service accessors declaration */

        var getFloorsByBuilding = function (building) {
            return $q.when(floors);
        };

        return {
            getFloorsByBuilding: getFloorsByBuilding
        };

    }

})();
