(function () {

    angular
        .module('mapping')
        .service('PlugService', PlugService);

    function PlugService($q) {

        /* FIXME: should be server side */

        var plugs = [];

        for (var i = 167; i <= 206; ++i) {
            plugs.push({
                name: 'D1-' + i,
                id: '1' + i,
                roomId: 5,
                position: {y: 6 - Math.floor((i - 167) / 8), x: (i - 167) % 8 + (((i - 167) % 8 >= 4) ? 1 : 0)},
                size: {x: 1, y: 1},
                group: {name: "Etudiant", id: 1, color: "#f2f2f2"}
            })
        }

        plugs.push({
            name: 'D1-207',
            id: 1207,
            roomId: 5,
            position: {x: 0, y: 0},
            size: {x: 1, y: 1},
            group: {name: "Professeur", id: 2, color: "#ffcccc"}
        })

        plugs.push({
            name: 'D1-208',
            id: 1208,
            roomId: 5,
            position: {x: 1, y: 0},
            size: {x: 1, y: 1},
            group: {name: "Autre", id: 3, color: "#ccccff"}
        })

        plugs.push({
            name: 'D1-209',
            id: 1209,
            roomId: 5,
            position: {x: 2, y: 0},
            size: {x: 1, y: 1},
            group: {name: "Autre", id: 3, color: "#ccccff"}
        })

        /* Service accessors declaration */

        var getPlugsByRoom = function (room) {
            return $q.when(plugs);
        }

        return {
            getPlugsByRoom: getPlugsByRoom
        };

    }

})();
