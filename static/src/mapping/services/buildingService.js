(function () {

    angular
        .module('mapping')
        .service('BuildingService', BuildingService);

    function BuildingService($q) {

        /* FIXME: should be server side */

        var buildings = [
            {
                name: 'EISTI Pau',
                id: 0
            }
        ];

        /* Service accessors declaration */

        var getBuildings = function () {
            return $q.when(buildings);
        };

        /* Return the accessors */

        return {
            getBuildings: getBuildings
        };

    }

})();
