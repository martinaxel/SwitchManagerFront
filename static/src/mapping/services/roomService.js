(function () {

    angular
        .module('mapping')
        .service('RoomService', RoomService);

    function RoomService($q) {

        /* FIXME: should be server side */

        var rooms = [
            {
                name: 'E101',
                id: 1,
                floorId: 1,
                position: {x: 6, y: 0},
                size: {x: 2, y: 1},
                group: {name: "Etudiant", id: 1, color: '#f2f2f2'}
            },
            {
                name: 'E102',
                id: 2,
                floorId: 1,
                position: {x: 4, y: 0},
                size: {x: 2, y: 1},
                group: {name: "Etudiant", id: 1, color: '#f2f2f2'}
            },
            {
                name: 'E103',
                id: 3,
                floorId: 1,
                position: {x: 3, y: 0},
                size: {x: 1, y: 1},
                group: {name: "Etudiant", id: 1, color: '#f2f2f2'}
            },
            {
                name: 'E104',
                id: 4,
                floorId: 1,
                position: {x: 1, y: 0},
                size: {x: 2, y: 1},
                group: {name: "Etudiant", id: 1, color: '#f2f2f2'}
            },
            {
                name: 'E105',
                id: 5,
                floorId: 1,
                position: {x: 0, y: 0},
                size: {x: 1, y: 2},
                group: {name: "Etudiant", id: 1, color: '#f2f2f2'}
            },
            {
                name: 'E106',
                id: 6,
                floorId: 1,
                position: {x: 0, y: 2},
                size: {x: 1, y: 2},
                group: {name: "Etudiant", id: 1, color: '#f2f2f2'}
            },
            {
                name: 'E107',
                id: 7,
                floorId: 1,
                position: {x: 2, y: 2},
                size: {x: 1, y: 2},
                group: {name: "Etudiant", id: 1, color: '#f2f2f2'}
            },
            {
                name: 'E108',
                id: 8,
                floorId: 1,
                position: {x: 0, y: 4},
                size: {x: 1, y: 2},
                group: {name: "Etudiant", id: 1, color: '#f2f2f2'}
            },
            {
                name: 'E109',
                id: 9,
                floorId: 1,
                position: {x: 1, y: 5},
                size: {x: 2, y: 1},
                group: {name: "Etudiant", id: 1, color: '#f2f2f2'}
            },
            {
                name: 'E110',
                id: 10,
                floorId: 1,
                position: {x: 3, y: 5},
                size: {x: 2, y: 1},
                group: {name: "Etudiant", id: 1, color: '#f2f2f2'}
            }
        ];

        /* Service accessors declaration */

        var getRoomsByFloor = function (floor) {
            return $q.when(rooms);
        }

        var getRoomById = function (id) {
            var res = undefined;

            for (var i = 0; i < rooms.length; ++i) {
                console.log(i, rooms[i]);
                if (rooms[i].id == id) {
                    res = rooms[i];
                    break;
                }
            }

            return $q.when(res);
        }

        return {
            getRoomsByFloor: getRoomsByFloor,
            getRoomById: getRoomById
        };

    }

})();
