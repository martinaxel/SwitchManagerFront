(function () {

    angular
        .module('visualization')
        .controller('VisualizationController', VisualizationController);

    function VisualizationController($scope, $mdSidenav, $mdMedia, BuildingService, FloorService, RoomService, PlugService, SelectionService) {

        /** Current selection variables **/

        var data = {
            buildings: [{name: "EISTI Pau", id: 0}],
            floors: undefined,
            rooms: undefined,
            plugs: undefined
        }

        var selected = {
            building: 0,
            floor: -1,
            room: -1,
            plug: -1
        }

        var generated = {
            filledRooms: undefined,
            filledPlugs: undefined
        }

        /** Functions to change the selection **/

        function changeBuilding(building) {
            selected.building = data.buildings.indexOf(building);

            // Reset other selections
            selected.floor = -1;
            selected.room = -1;
            selected.plug = -1;

            // Load the building's floors and select a default one
            FloorService
                .getFloorsByBuilding(building).then(function (floors) {
                    data.floors = floors;

                    changeFloor(floors[0]);
                });
        }

        function changeFloor(floor) {
            selected.floor = data.floors.indexOf(floor);

            // Reset other selections
            selected.room = -1;
            selected.plug = -1;

            // Load the floor's rooms
            RoomService
                .getRoomsByFloor(floor).then(function (rooms) {
                    data.rooms = rooms;

                    generated.filledRooms = fillWithEmptyBlocks(rooms);
                });
        }

        function changeRoom(room) {
            selected.room = data.rooms.indexOf(room);

            // Reset other selections
            selected.plug = -1;

            // Load the room's plugs
            PlugService
                .getPlugsByRoom(room).then(function (plugs) {
                    data.plugs = plugs;

                    generated.filledPlugs = fillWithEmptyBlocks(plugs);
                    console.log(generated.filledPlugs);
                })
        }

        function onPlugSelection(plug) {
            for (var i = 0; i < plugSelection.length; ++i) {
                if (plugSelection[i].room.id == plug.roomId) {
                    // FIXME : si une prise y est déjà, ça crash :>
                    plugSelection[i].selectedPlugs.push(plug);
                    return;
                }
            }

            RoomService.getRoomById(plug.roomId).then(function (room) {
                plugSelection.push({
                    room: room,
                    toggled: false,
                    selectedPlugs: [plug]
                });
            })
        }

        function toggleExpandSelectionTab(element) {
            element.toggled = !element.toggled;
        }

        /** Initializers **/

        BuildingService
            .getBuildings().then(function (buildings) {
                data.buildings = buildings;

                // Change the building to the default one
                changeBuilding(buildings[0]);
            })

        /** Scope data binding **/
        $scope.data = data;
        $scope.selected = selected;
        $scope.generated = generated;
        $scope.plugSelection = SelectionService.getSelection();

        /** Scope functions binding **/
        $scope.changeBuilding = changeBuilding;
        $scope.changeFloor = changeFloor;
        $scope.changeRoom = changeRoom;
        $scope.onPlugSelection = SelectionService.toggleSelection;
        $scope.isPlugSelected = SelectionService.isPlugInSelection;
        $scope.isRoomSelected = SelectionService.isRoomInSelection;
        $scope.toggleExpandSelectionTab = toggleExpandSelectionTab;

        /** Common view bindings **/
        $scope.$mdMedia = $mdMedia;

        /** Common view interactions **/
        $scope.toggleSidenav = function(menuId) {
            $mdSidenav(menuId).toggle();
        };

    }

    /* Utils */

    function getMaxColumn(data) {
        var max = 0;

        for (var i = 0; i < data.length; ++i)
            if (data[i].position.x + data[i].size.x > max)
                max = data[i].position.x + data[i].size.x;

        return max;
    }

    function getMaxRow(data) {
        var max = 0;

        for (var i = 0; i < data.length; ++i)
            if (data[i].position.y + data[i].size.y > max)
                max = data[i].position.y + data[i].size.y;

        return max;
    }

    /**
     * @function fillWithEmtpyBlocks
     * @brief Fill a 2D representation with empty blocks to be printable in a md-grid-list
     * @param data List containing the objects to fill.
     * @return A list of filled data.
     *
     * NOTE: in order to be fillable, each element in the data list MUST at least contains the following attributes :
     *      {
     *          position: {x: <x position>, y: <y position>},
     *          size: {x: <x size>, y: <y size>}
     *          // Any other attributes.
     *      }
     */
    function fillWithEmptyBlocks(data) {

        var nrow = getMaxRow(data);
        var ncol = getMaxColumn(data);
        var toAppend = [];

        // Sort the data
        data = data.sort(function (a, b) {
            if (a.position.y != b.position.y)
                return a.position.y - b.position.y;
            else
                return a.position.x - b.position.x;
        });

        for (var i = 0; i < nrow; ++i) {

            var inRow = data.filter(function (e) {
                return e.position.y <= i && i < e.position.y + e.size.y;
            });

            // Fill inner spaces
            for (var j = 1; j < inRow.length; ++j) {
                if (inRow[j - 1].position.x + inRow[j - 1].size.x < inRow[j].position.x) {
                    var posx = inRow[j - 1].position.x + inRow[j - 1].size.x;

                    toAppend.push({
                        gridTileType: 'empty',
                        position: {x: posx, y: i},
                        size: {x: inRow[j].position.x - posx, y: 1}
                    });
                }
            }

            // Fill outer spaces
            var last = inRow[inRow.length - 1];
            if (last && last.position.x + last.size.x < ncol) {
                toAppend.push({
                    gridTileType: 'empty',
                    position: {x: last.position.x + last.size.x, y: i},
                    size: {x: ncol - last.position.x - last.size.x, y: 1}
                });
            }

        }

        // Return sorted result
        result = data.concat(toAppend).sort(function (a, b) {
            if (a.position.y != b.position.y)
                return a.position.y - b.position.y;
            else
                return a.position.x - b.position.x;
        });


        return {
            data: result,
            ncol: ncol,
            nrow: nrow
        }
    }
})();
