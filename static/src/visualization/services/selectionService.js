(function () {

    angular
        .module('visualization')
        .service('SelectionService', ['RoomService', SelectionService]);

    function SelectionService(RoomService) {

        console.log("Hello !");

        var currentSelection = [];

        function getAssociatedRoom(plug) {
            return currentSelection.filter(function (element) {
                return plug.roomId == element.room.id;
            });

        }

        function addToSelection(plug) {
            var associatedRoom = getAssociatedRoom(plug);

            if (associatedRoom.length) {
                associatedRoom[0].selectedPlugs.push(plug);
            } else {
                RoomService
                    .getRoomById(plug.roomId)
                    .then(function (room) {
                        currentSelection.push({
                            toggled: false,
                            room: room,
                            selectedPlugs: [plug]
                        });

                        console.log(currentSelection);
                    });
            }
        }

        function removeFromSelection(plug) {
            var associatedRoom = getAssociatedRoom(plug);

            if (associatedRoom.length) {
                for (var i = 0; i < associatedRoom[0].selectedPlugs.length; ++i) {
                    if (associatedRoom[0].selectedPlugs[i].id == plug.id) {
                        associatedRoom[0].selectedPlugs.splice(i, 1);
                        break;
                    }
                }

                if (!associatedRoom[0].selectedPlugs.length) {
                    currentSelection.splice(currentSelection.indexOf(associatedRoom[0]), 1);
                }
            }
        }

        function isPlugInSelection(plug) {
            var associatedRoom = getAssociatedRoom(plug);

            if (associatedRoom.length) {
                var p = associatedRoom[0].selectedPlugs.filter(function (element) {
                    return element.id == plug.id;
                });

                if (p.length) {
                    return true;
                }
            }

            return false;
        }

        function isRoomInSelection(room) {
            return currentSelection.filter(function (element) {
                return element.room.id == room.id;
            }).length != 0;
        }

        function toggleSelection(plug) {
            if (isPlugInSelection(plug))
                removeFromSelection(plug);
            else
                addToSelection(plug);
        }

        function getSelection() {
            return currentSelection;
        }

        return {
            addToSelection: addToSelection,
            removeFromSelection: removeFromSelection,
            isPlugInSelection: isPlugInSelection,
            isRoomInSelection: isRoomInSelection,
            toggleSelection: toggleSelection,
            getSelection: getSelection
        }
    }

})();
