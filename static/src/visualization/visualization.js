(function () {
    "use strict";

    angular
        .module('visualization', ['mapping'])
        .directive('smVisualizationSubselect', Subselect)
        .directive('smVisualizationBlockSelection', BlockSelection);

    function Subselect() {
        return {
            scope: {
                data: "=data",
                selected: "=selected",
                onChange: "=onChange"
            },
            templateUrl: "static/src/visualization/smVisualizationSubselect.tmpl.html"
        }
    }

    function BlockSelection() {

        return {
            scope: {
                data: "=data",
                onSelect: "=onSelect",
                emphaseFunc: "=emphaseFunction"
            },
            templateUrl: "static/src/visualization/smVisualizationBlockSelection.tmpl.html"
        }
    }

})();
