(function () {

    angular
        .module('dashboard')
        .controller('DashboardController', DashboardController);

    function DashboardController($scope, $mdSidenav, $mdMedia) {

        // Sub view selectable items
        $scope.selectable = [
            {
                name: "Temporary configuration online",
                id: "temp-configs"
            },
            {
                name: "Scheduled configurations",
                id: "scheduled-configs"
            },
            {
                name: "Last actions",
                id: "last-actions"
            }
        ];

        // Current selected view
        $scope.selected = 0;

        /** Common view bindings **/

        $scope.$mdMedia = $mdMedia;

        /** Common view interactions **/

        $scope.toggleSidenav = function(menuId) {
            $mdSidenav(menuId).toggle();
        };
    }

})();
