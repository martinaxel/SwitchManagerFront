(function () {

    angular
        .module('ui')
        .service('SearchService', SearchService);

    function SearchService($q) {

        /* FIXME: should be server side */

        var searchs = [
            {
                name: "EISTI Pau",
                type: "building",
                more: "",
                id: 0
            },
            {
                name: "E101",
                type: "room",
                mode: "floor 1",
                id: 1
            }
        ];

        var getSearchEntries = function () {
            return $q.when(searchs);
        };

        return {
            getSearchEntries: getSearchEntries
        };
    }
})();
