(function () {

    angular
        .module('ui')
        .controller('MainMenuController', MainMenuController);

    function MainMenuController($scope) {

        $scope.sections = [
            {
                name: 'Dashboard',
                type: 'link',
                href: '#/dashboard',
                icon: 'dashboard'
            },
            {
                name: 'Configurations',
                type: 'subheader'
            },
            {
                name: 'Planning',
                type: 'link',
                href: '#/planning',
                icon: 'today'
            },
            {
                name: 'Search by rooms',
                type: 'link',
                href: '#/visualization',
                icon: 'search'
            },
            {
                name: 'Re-schedule',
                type: 'link',
                href: '#/configuration/schedule',
                icon: 'schedule'
            },
            {
                name: 'Manage configuration',
                type: 'link',
                href: '#/configurations/manage',
                icon: 'build'
            },
            {
                name: 'Administration',
                type: 'subheader'
            },
            {
                name: 'Change mapping',
                type: 'link',
                href: '#/mapping/manage',
                icon: 'dns'
            },
            {
                name: 'Setup',
                type: 'link',
                href: '#/admin',
                icon: 'settings'
            }
        ];

    }

})();
