(function () {

    angular
        .module('ui', [])
        .directive('smUiSearchBar', ['SearchService', UiSearchBar]);

    function UiSearchBar(SearchService) {
        return {
            scope: {
                onChange: "=onChange"
            },
            link: function (scope, element, attrs) {
                scope.searchItems = SearchService.getSearchEntries();
            },
            templateUrl: "static/src/ui/smUISearchBar.tmpl.html"
        }
    }

})();
