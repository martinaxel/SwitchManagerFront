(function () {

    angular
        .module('switchManager', ['ngMaterial', 'ngRoute', 'ngMdIcons', 'ui', 'dashboard', 'mapping', 'visualization'])
        .config(function($mdThemingProvider, $mdIconProvider, $routeProvider) {

            $mdIconProvider
                .defaultIconSet('static/assets/svg/avatars.svg', 128);

            $routeProvider
                .when('/dashboard', {
                    templateUrl: 'static/src/dashboard/dashboard.html',
                    controller: 'DashboardController'
                })
                .when('/visualization', {
                    templateUrl: 'static/src/visualization/visualization.html',
                    controller: 'VisualizationController'
                })
                .otherwise('/dashboard')

        })
})();
